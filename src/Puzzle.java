import java.util.*;
import java.util.StringTokenizer;
import java.util.regex.Pattern;

public class Puzzle {
	//https://gist.github.com/vo/2481737

	public String getWords() {
		Scanner sc = new Scanner(System.in);
		String s1;
		String s2;
		String s3;
		String asd;
		String asd2;
		StringBuffer sb = new StringBuffer();
		StringBuffer sb2 = new StringBuffer();
		System.out.println("Input first word");
		s1 = sc.nextLine();
		s1.toString();
		sb.append(s1.toUpperCase() + " + ");
		sb2.append(s1.toUpperCase() + " ");
		System.out.println("Input second word");
		s2 = sc.nextLine();
		s2.toString();
		sb.append(s2.toUpperCase() + " = ");
		sb2.append(s2.toUpperCase() + " ");
		System.out.println("input third word");
		s3 = sc.nextLine();
		s3.toString();
		if(s3.length()>18 || s2.length()>18 || s1.length()>18) throw new IndexOutOfBoundsException("One of the words is too long. They shouldn't be longer than 18 digits." + " First word: " + s1 + " Second word " + s2 + " Third word " + s3);
		if( Pattern.matches(".*[a-zA-Z]+.*[a-zA-Z]", s1) &&  Pattern.matches(".*[a-zA-Z]+.*[a-zA-Z]", s2) &&  Pattern.matches(".*[a-zA-Z]+.*[a-zA-Z]", s3) )
		 {
		   
		 }
		else 
		{
		  throw new IndexOutOfBoundsException("Words contain illegal characters" +" First word: " + s1 + " Second word " + s2 + " Third word " );
		}
		sb.append(s3.toUpperCase());
		sb2.append(s3.toUpperCase());
		asd = sb.toString();
		asd2 = sb2.toString();
		System.out.println(asd);
		return asd2;
	}	
	
	
	
	
	private static long eval(String str) {
		long val = 0;
		StringTokenizer st = new StringTokenizer(str, " ", true);
		while (st.hasMoreTokens()) {
			String next = st.nextToken();
			if (next.equals(" ")) {
				val += Long.parseLong(st.nextToken().trim());
			} else {
				val =  Long.parseLong(next.trim());
			}
		}

		return val;
	}

	private static String solve(String str) {
		char c = 0;
		for (int i = 0; i < str.length(); i++) {
			if (Character.isAlphabetic(str.charAt(i))) {
				c = str.charAt(i);
				break;
			}
		}
		if (c == 0) {
			String[] ops = str.split(" ");
			long o1 = eval(ops[0] + " " + ops[1]);
			long o2 = eval(ops[2]);
			if (o1 == o2) {
				return str;
			} else {
				return "";
			}
		} else {
			char[] dset = new char[10];
			for (int i = 0; i < str.length(); i++) {
				if (Character.isDigit(str.charAt(i))) {
					dset[str.charAt(i) - '0'] = 1;
				}
			}
			for (int i = 0; i < 10; i++) {
				if (dset[i] == 0) {
					int index = str.indexOf(c);
					if (index > 0) {
						char prev = str.charAt(index - 1);
						if (prev == ' ') {
							i++;
						}
					}
					if (i == 0 && index == 0) {
						i++;
					}

					String r = solve(str.replace(String.valueOf(c), String.valueOf(i)));
					if (!r.isEmpty()) {
						return r;
					}
				}
			}
		}
		return "";
	}

	public static void main(String[] args) {
		//Puzzle puz= new Puzzle();
		String res = solve("ABCDEFGHIJAB ABCDEFGHIJA ACEHJBDFGIAC");
		if(res=="") {
			System.out.println("There are no solutions");
		}else{
		System.out.println("result: " + res);
		}
	}
}



//https://gist.github.com/vo/2481737